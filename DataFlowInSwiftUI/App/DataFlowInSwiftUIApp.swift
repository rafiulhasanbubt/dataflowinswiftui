//
//  DataFlowInSwiftUIApp.swift
//  DataFlowInSwiftUI
//
//  Created by rafiul hasan on 12/12/21.
//

import SwiftUI

@main
struct DataFlowInSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
