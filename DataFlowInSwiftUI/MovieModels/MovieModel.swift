//
//  Movie.swift
//  DataFlowInSwiftUI
//
//  Created by rafiul hasan on 12/12/21.
//

import SwiftUI
import Foundation

struct MovieModel: Codable {
  let title: String
  let genre: String
  let rating: Double
}

extension MovieModel {
  static var possibleGenres: [String] {
    [
      "Action",
      "Adventure",
      "Crime",
      "Comedy",
      "Drama",
      "Family",
      "Fantasy",
      "Horror",
      "Mystery",
      "Romance",
      "Sci-Fi",
      "Thriller",
      "Western"
    ]
  }
}
