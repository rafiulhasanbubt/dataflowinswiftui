//
//  UserView.swift
//  DataFlowInSwiftUI
//
//  Created by rafiul hasan on 12/12/21.
//

import SwiftUI

struct UserView: View {
    @EnvironmentObject var userStore: UserStore
    @State private var userName = ""
    @State private var favoriteGenre = ""
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        NavigationView {
            Form {
                Section(header: Text("User")) {
                    TextField("User Name", text: $userName)
                }
                
                Section(header: Text("Favorite Genre")) {
                  GenrePickerView(genre: $favoriteGenre)
                }
            }
            .navigationBarTitle(Text("\(userName) Info"), displayMode: .inline)
            .navigationBarItems( trailing: Button(action: updateUserInfo) {
                Text("Update")
            })
            .onAppear {
              userName = userStore.currentUserInfo?.userName ?? ""
              favoriteGenre = userStore.currentUserInfo?.favoriteGenre ?? ""
            }
        }
    }
    
    func updateUserInfo() {
        let newUserInfo = UserInfo(userName: userName, favoriteGenre: favoriteGenre)
        userStore.currentUserInfo = newUserInfo
        presentationMode.wrappedValue.dismiss()
    }
}

struct UserView_Previews: PreviewProvider {
    static var previews: some View {
        UserView()
    }
}
