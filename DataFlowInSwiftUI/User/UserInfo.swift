//
//  UserInfo.swift
//  DataFlowInSwiftUI
//
//  Created by rafiul hasan on 12/12/21.
//

import Foundation

struct UserInfo {
    let userName: String
    let favoriteGenre: String
}
