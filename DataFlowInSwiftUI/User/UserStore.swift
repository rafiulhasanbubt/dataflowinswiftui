//
//  UserStore.swift
//  DataFlowInSwiftUI
//
//  Created by rafiul hasan on 12/12/21.
//

import Foundation
import Combine

class UserStore: ObservableObject {
  @Published var currentUserInfo: UserInfo?
}
