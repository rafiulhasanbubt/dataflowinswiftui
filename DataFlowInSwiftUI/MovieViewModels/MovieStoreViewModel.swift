//
//  MovieStoreViewModel.swift
//  DataFlowInSwiftUI
//
//  Created by rafiul hasan on 12/12/21.
//

import SwiftUI
import Foundation

class MovieStoreViewModel: ObservableObject {
    static let moviesKey = "Movies"
    static let defaultMovies = [
        MovieModel(title: "The Matrix", genre: "Action, Sci-Fi", rating: 4.0),
        MovieModel(title: "Airplane!", genre: "Comedy", rating: 4.5),
        MovieModel(title: "The Dark Knight", genre: "Action", rating: 4.5)
    ]
    
    static func loadMovies() -> [MovieModel] {
        let savedMovies = UserDefaults.standard.object(forKey: MovieStoreViewModel.moviesKey)
        if let savedMovies = savedMovies as? Data {
            let decoder = JSONDecoder()
            return (try? decoder.decode([MovieModel].self, from: savedMovies)) ?? MovieStoreViewModel.defaultMovies
        }
        return MovieStoreViewModel.defaultMovies
    }
    
    @Published var movies = loadMovies() {
        didSet {
            persistMovies()
        }
    }
    
    func addMovie(title: String, genre: String, rating: Double) {
        let newMovie = MovieModel(title: title, genre: genre, rating: rating)
        movies.append(newMovie)
    }
    
    func deleteMovie(at offsets: IndexSet) {
        movies.remove(atOffsets: offsets)
    }
    
    private func persistMovies() {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(movies) {
            UserDefaults.standard.set(encoded, forKey: MovieStoreViewModel.moviesKey)
        }
    }
}
