//
//  AddMovieView.swift
//  DataFlowInSwiftUI
//
//  Created by rafiul hasan on 12/12/21.
//

import SwiftUI

struct AddMovieView: View {
    static let defaultMovieTitle = "An untitled masterpiece"
    static let defaultMovieGenre = MovieModel.possibleGenres.first ?? "Genre-buster"
    
    @Environment(\.presentationMode) var presentationMode
    let movieStore: MovieStoreViewModel
    @EnvironmentObject var userStore: UserStore
    @Binding var showModal: Bool
    @State private var title = ""
    @State private var genre = ""
    @State private var rating: Double = 0
    
    var body: some View {
        NavigationView {
            Form {
                Section(header: Text("Title")) {
                    TextField("Title", text: $title)
                }
                Section(header: Text("Genre")) {
                    Picker(selection: $genre, label: Spacer()) {
                        ForEach(MovieModel.possibleGenres, id: \.self) {
                            Text($0)
                        }
                    }.pickerStyle(WheelPickerStyle())
                }
                Section(header: Text("Rating")) {
                    Slider(value: $rating, in: 0...5, step: 0.5)
                    RatingView(rating: rating)
                }
            }
            .navigationBarTitle(Text("Add Movie"), displayMode: .inline)
            .navigationBarItems(leading: Button(action: dismiss) { Image(systemName: "xmark") }, trailing: Button(action: addMovie) { Text("Add")
            })
        }.onAppear { genre = userStore.currentUserInfo?.favoriteGenre ?? "" }
    }
    
    private func addMovie() {
        movieStore.addMovie(
            title: title.isEmpty ? AddMovieView.defaultMovieTitle : title,
            genre: genre.isEmpty ? AddMovieView.defaultMovieGenre : genre, rating: rating)
        
        showModal.toggle()
    }
    
    private func dismiss() {
        presentationMode.wrappedValue.dismiss()
    }
}

struct AddMovieView_Previews: PreviewProvider {
    static var previews: some View {
        AddMovieView(movieStore: MovieStoreViewModel(), showModal: .constant(true))
    }
}
