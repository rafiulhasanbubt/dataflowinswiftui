//
//  MovieRow.swift
//  DataFlowInSwiftUI
//
//  Created by rafiul hasan on 12/12/21.
//

import SwiftUI

struct MovieRow: View {
    let movie: MovieModel
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(movie.title)
                    .font(.title)
                Text(movie.genre)
                    .font(.caption)
            }
            
            Spacer()
            
            VStack(alignment: .trailing) {
                Spacer()
                RatingView(rating: movie.rating)
            }
        }
    }
}

struct MovieRow_Previews: PreviewProvider {
    static var previews: some View {
        MovieRow(movie: MovieModel(title: "The Forest Man", genre: "Threler", rating: 4.0))
    }
}
