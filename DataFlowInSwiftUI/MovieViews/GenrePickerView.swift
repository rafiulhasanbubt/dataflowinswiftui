//
//  GenrePickerView.swift
//  DataFlowInSwiftUI
//
//  Created by rafiul hasan on 12/12/21.
//

import SwiftUI

struct GenrePickerView: View {
    @Binding var genre: String
    
    var body: some View {
        Picker(selection: $genre, label: Spacer()) {
            ForEach(MovieModel.possibleGenres, id: \.self) {
                Text($0)
            }
        }
        .pickerStyle(WheelPickerStyle())
    }
}

struct GenrePickerView_Previews: PreviewProvider {
    static var previews: some View {
        GenrePickerView(genre: .constant("Action"))
    }
}
