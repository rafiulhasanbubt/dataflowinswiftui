//
//  MovieListView.swift
//  DataFlowInSwiftUI
//
//  Created by rafiul hasan on 12/12/21.
//

import SwiftUI

struct MovieListView: View {
    @EnvironmentObject var userStore: UserStore
    @ObservedObject var movieStore = MovieStoreViewModel()    
    @State private var isPresented = false
    
    var body: some View {
        NavigationView {
            List {
                ForEach(movieStore.movies, id: \.title) {
                    MovieRow(movie: $0)
                }
                .onDelete(perform: movieStore.deleteMovie)
            }
            .sheet(isPresented: $isPresented) {
                AddMovieView(movieStore: movieStore, showModal: $isPresented)
            }
            .navigationBarTitle(Text("Favorit Flicks"))
            .navigationBarItems( leading: NavigationLink(destination: UserView()) { HStack {
                userStore.currentUserInfo.map { Text($0.userName) }
                Image(systemName: "person.fill")
            }
            }, trailing: Button(action: { isPresented.toggle() }) {
                Image(systemName: "plus")
            }
            )
        }
    }
}

struct MovieListView_Previews: PreviewProvider {
  static var previews: some View {
    MovieListView(movieStore: MovieStoreViewModel())
  }
}
